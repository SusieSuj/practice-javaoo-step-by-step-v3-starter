package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    protected List<Klass> klassList = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder klassName = new StringBuilder();
        if (klassList.size()!=0)
        {
            for (int i = 0; i < klassList.size(); i++) {
                klassName.append(klassList.get(i).number);
                if (i< klassList.size()-1)
                    klassName.append(", ");
            }

            return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.",this.name,this.age, klassName);
        }
        else
            return String.format("My name is %s. I am %d years old. I am a teacher.",this.name,this.age);
    }

    public void assignTo(Klass klass){
        klassList.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return  klassList.stream().filter(klass1 -> klass1.equals(klass)).findAny().isPresent();
    }

    public boolean isTeaching(Student student){
        if (klassList.contains(student.klass))
            return true;
        return false;
    }
}
