package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    public int number;
    protected Student leader;

    protected List<Person> teacherAndStudent = new ArrayList<>();

    public Klass(int number){
        this.number=number;
    }

    @Override
    public boolean equals(Object o) {
        if(o.equals(number)) {
            return true;
        }else {
            return false;
        }
    }

    public void assignLeader(Student student){
        if (student.klass.number!=this.number){
            System.out.println("It is not one of us.");
        }
        this.leader=student;
        for (int i = 0; i < teacherAndStudent.size(); i++) {
            String identity=teacherAndStudent.get(i).getClass().getSimpleName().toLowerCase();
            System.out.println(String.format("I am %s, %s of Class %d. I know %s become Leader.",
                    teacherAndStudent.get(i).name,identity,this.number,this.leader.name));
        }
    }

    public boolean isLeader(Student student){
        return  this.leader.equals(student);
    }

    public void attach(Person person){
        teacherAndStudent.add(person);
    }
}
