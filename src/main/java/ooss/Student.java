package ooss;

public class Student extends Person {
    protected Klass klass=new Klass(-1);

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        if (this.klass.number!=-1 && klass.isLeader(new Student(this.id,this.name,this.age)))
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",this.name,this.age,this.klass.number);
       else if (this.klass.number!=-1 )
            return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",this.name,this.age,this.klass.number);
        else
            return String.format("My name is %s. I am %d years old. I am a student.",this.name,this.age);


    }

    public void join(Klass kclass){
        this.klass=kclass;
    }

    public boolean isIn(Klass klass){
        return this.klass.equals(klass);
    }
}
